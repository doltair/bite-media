# FROM ruby:2.7-slim
FROM ruby:2.7
# Install and prepare Rails App

USER root

 RUN apt-get update ;
 RUN apt-get install -y --no-install-recommends build-essential \
  zlib1g-dev liblzma-dev patch libmariadb-dev \
  libjemalloc-dev \
  libjemalloc2 \
  curl \
  bash \
  git \
  nodejs ; \
  rm -rf /var/lib/apt/lists/*

ENV LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2
ENV APP_HOME /app
ENV APP_USER bite
RUN useradd -ms /bin/bash $APP_USER
RUN mkdir $APP_HOME
RUN mkdir -p $APP_HOME/gem_store/.bundle
 
ENV BUNDLE_PATH $APP_HOME/gem_store
ENV BUNDLE_USER_HOME $APP_HOME/gem_store/.bundle
ENV GEM_PATH $APP_HOME/gem_store
ENV GEM_HOME $APP_HOME/gem_store
ADD . $APP_HOME


WORKDIR $APP_HOME
COPY Gemfile Gemfile

RUN bundle config set --local without 'development test'
RUN bundle config timeout 30
RUN bundle
## Clean
RUN rm -rf /usr/local/bundle/cache/*.gem

RUN chown -R $APP_USER $APP_HOME
USER  $APP_USER

LABEL maintainer="santosgian28@gmail.com"

EXPOSE 8080
CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "8080", "./config.ru"]
